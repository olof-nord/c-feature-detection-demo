SHELL = /bin/bash

# specify C standard
STD := -std=c11
# specify C compiler
CC := gcc $(STD)
# specify compile flags
CFLAGS := -Wall -Wextra -pedantic -g

BUILD_DIR := build
SOURCE_DIR := src

# Find all the C files we want to compile
SOURCES := $(shell find $(SOURCE_DIR) -name '*.c')

# String substitution for every C file.
# As an example, hello.c turns into /build/hello.c.o
OBJECTS := $(SOURCES:%=$(BUILD_DIR)/%.o)

# Name of final executable
BIN = demo

# Build step for object files
$(BUILD_DIR)/$(BIN): $(OBJECTS)
	$(CC) $(OBJECTS) -o $@

# Build step for C source
$(BUILD_DIR)/%.c.o: %.c
	mkdir -p $(dir $@)
	$(CC) $(CFLAGS) -c $< -o $@

.PHONY : run
run:
	@$(BUILD_DIR)/$(BIN)

.PHONY : clean
clean:
	rm -rf $(BUILD_DIR)
