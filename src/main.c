#include <stdio.h>

int main() {
  #ifdef __linux__
    printf("Linux OS detected\n");
  #else
    printf("Linux OS NOT detected!\n");
  #endif

  #ifdef __x86_64__
    printf("x86_64 OS detected\n");
  #else
    printf("x86_64 OS NOT detected!\n");
  #endif

  // Note that there is no "__riscv__" (underscores on both sides) macro defined.
  #ifdef __riscv
    printf("riscv OS detected\n");
  #else
    printf("riscv OS NOT detected!\n");
  #endif

  return 0;
}
