# c-feature-detection-demo

This is a small demo program to check the operating system of the host using C pre-processor macros defined by the
compiler.

## Run

To build and execute, GCC and Make are required.

```shell
make && make run
```

## C preprocessor macros

## x86_64

Here are all currently defined macros for x86_64 listed.

Linux 6.0.10-arch2-1 #1 SMP PREEMPT_DYNAMIC Sat, 26 Nov 2022 16:51:18 +0000 x86_64 GNU/Linux

cpp (GCC) 12.2.0

```shell
$ cpp -dM /dev/null | grep x86_64
#define __x86_64 1
#define __x86_64__ 1
```

```shell
$ make run
Linux OS detected
x86_64 OS detected
riscv OS NOT detected!
```

### RISC-V

Here are all currently defined macros for riscv64 listed.

Linux 5.19.0-1006-generic #7-Ubuntu SMP Sat Oct 15 04:27:01 UTC 2022 riscv64 riscv64 riscv64 GNU/Linux

cpp (Ubuntu 12.2.0-3ubuntu1) 12.2.0

```shell
$ make run
Linux OS detected
x86_64 OS NOT detected!
riscv OS detected
```

```shell
$ cpp -dM /dev/null | grep riscv
#define __riscv 1
#define __riscv_atomic 1
#define __riscv_cmodel_medany 1
#define __riscv_fdiv 1
#define __riscv_float_abi_double 1
#define __riscv_mul 1
#define __riscv_muldiv 1
#define __riscv_xlen 64
#define __riscv_fsqrt 1
#define __riscv_m 2000000
#define __riscv_a 2001000
#define __riscv_c 2000000
#define __riscv_d 2002000
#define __riscv_f 2002000
#define __riscv_i 2001000
#define __riscv_zicsr 2000000
#define __riscv_compressed 1
#define __riscv_flen 64
#define __riscv_arch_test 1
#define __riscv_div 1
#define __riscv_zifencei 2000000
#define __riscv_cmodel_pic 1
```

## References

* [Anyway to see list of preprocessor defined macros?](https://stackoverflow.com/questions/10904873/anyway-to-see-list-of-preprocessor-defined-macros)
* [Makefile Cookbook](https://makefiletutorial.com/#makefile-cookbook)
